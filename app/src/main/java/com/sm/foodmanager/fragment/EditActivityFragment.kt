package com.sm.foodmanager.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.fragment.findNavController
import com.sm.foodmanager.R
import com.sm.foodmanager.utils.kCalFormula
import com.sm.foodmanager.utils.putStringInSP
import com.sm.foodmanager.utils.waterFormula


class EditActivityFragment : Fragment(R.layout.fragment_edit_activity) {
    private var rgChooseAct: RadioGroup? = null
    private var btnSave: AppCompatButton? = null

    private var prefs: SharedPreferences? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView(view)
        initListeners(view)
    }

    private fun initListeners(view: View) {
        prefs = context?.getSharedPreferences("FoodManager", Context.MODE_PRIVATE)

        btnSave?.setOnClickListener {
            val rbId: Int? = rgChooseAct?.checkedRadioButtonId

            if (rbId is Int) {
                putStringInSP(view, rbId, prefs, "activity")
            }

            val kCal = kCalFormula(
                prefs?.getString("sex", 0.toString())!!,
                prefs?.getString("age", 0.toString())!!,
                prefs?.getString("height", 0.toString())!!,
                prefs?.getString("weight", 0.toString())!!,
                prefs?.getString("activity", 0.toString())!!,
                prefs?.getString("wishes", 0.toString())!!,
            )
            prefs?.edit()?.putInt("kcal", kCal)?.apply()

            val water = waterFormula(
                prefs?.getString("sex", 0.toString())!!,
                prefs?.getString("weight", 0.toString())!!,
                prefs?.getString("activity", 0.toString())!!,
            )
            prefs?.edit()?.putInt("water", water)?.apply()

            findNavController().navigate(R.id.action_editActivityFragment_to_profileFragment)
        }
    }

    private fun findView(view: View) {
        rgChooseAct = view.findViewById(R.id.act_radio_group)
        btnSave = view.findViewById(R.id.btn_save)
    }
}