package com.sm.foodmanager.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import com.sm.foodmanager.R
import com.sm.foodmanager.utils.kCalFormula
import com.sm.foodmanager.utils.waterFormula

class EditAboutYouFragment : Fragment(R.layout.fragment_edit_about_you) {

    private var etAge: EditText? = null
    private var tiAge: TextInputLayout? = null
    private var etHeight: EditText? = null
    private var tiHeight: TextInputLayout? = null
    private var etWeight: EditText? = null
    private var tiWeight: TextInputLayout? = null
    private var btnNext: AppCompatButton? = null

    private var prefs: SharedPreferences? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView(view)
        initListeners()
    }

    private fun initListeners() {
        prefs = context?.getSharedPreferences("FoodManager", Context.MODE_PRIVATE)

        btnNext?.setOnClickListener {

            if (etAge != null)  prefs?.edit()?.putString("age", etAge?.text?.toString())?.apply()
            if (etHeight != null) prefs?.edit()?.putString("height", etHeight?.text?.toString())?.apply()
            if (etWeight != null) prefs?.edit()?.putString("weight", etWeight?.text?.toString())?.apply()

            val kCal = kCalFormula(
                prefs?.getString("sex", 0.toString())!!,
                prefs?.getString("age", 0.toString())!!,
                prefs?.getString("height", 0.toString())!!,
                prefs?.getString("weight", 0.toString())!!,
                prefs?.getString("activity", 0.toString())!!,
                prefs?.getString("wishes", 0.toString())!!,
            )
            prefs?.edit()?.putInt("kcal", kCal)?.apply()

            val water = waterFormula(
                prefs?.getString("sex", 0.toString())!!,
                prefs?.getString("weight", 0.toString())!!,
                prefs?.getString("activity", 0.toString())!!,
            )

            prefs?.edit()?.putInt("water", water)?.apply()

            findNavController().navigate(R.id.action_editAboutYouFragment_to_profileFragment)
        }
    }

    private fun findView(view: View) {
        etAge = view.findViewById(R.id.et_age)
        tiAge = view.findViewById(R.id.ti_age)
        etHeight = view.findViewById(R.id.et_height)
        tiHeight = view.findViewById(R.id.ti_height)
        etWeight = view.findViewById(R.id.et_weight)
        tiWeight = view.findViewById(R.id.ti_weight)
        btnNext = view.findViewById(R.id.btn_save)
    }
}
