package com.sm.foodmanager.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.sm.foodmanager.R


class StatisticsFragment : Fragment(R.layout.fragment_statistics) {

    lateinit var seriesWater: BarGraphSeries<DataPoint>
    lateinit var seriesKcal: BarGraphSeries<DataPoint>
    private var prefs: SharedPreferences? = null
    private var btnBack: Button? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView(view)

        seriesWater = BarGraphSeries<DataPoint>()
        var graphview: GraphView = view.findViewById(R.id.grapgWater) as GraphView
        seriesWater.setSpacing(50);
        seriesWater=WaterFragment.seriesWater

        seriesKcal = BarGraphSeries<DataPoint>()
        var graphview2: GraphView = view.findViewById(R.id.graphKcal) as GraphView
        seriesKcal.setSpacing(50);
        seriesKcal=KcalFragment.seriesKcal


        graphview.addSeries(seriesWater)
        graphview2.addSeries(seriesKcal)

        btnBack?.setOnClickListener {
            findNavController().navigate(R.id.action_statisticsFragment_to_profileFragment)
        }
    }

    override fun onResume() {

        super.onResume()
    }

    private fun findView(view: View) {
        btnBack = view.findViewById(R.id.btn_back)
    }

}