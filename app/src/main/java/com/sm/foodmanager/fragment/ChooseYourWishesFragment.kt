package com.sm.foodmanager.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sm.foodmanager.R
import com.sm.foodmanager.utils.kCalFormula
import com.sm.foodmanager.utils.putStringInSP
import com.sm.foodmanager.utils.waterFormula

class ChooseYourWishes : Fragment(R.layout.fragment_choose_your_wishes) {

    private var rgChooseWishes: RadioGroup? = null
    private var btnGo: AppCompatButton? = null
    private var prefs: SharedPreferences? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView(view)
        initListeners(view)

    }


    private fun initListeners(view: View) {
        prefs = context?.getSharedPreferences("FoodManager", Context.MODE_PRIVATE)

        btnGo?.setOnClickListener {
            val rbId: Int? = rgChooseWishes?.checkedRadioButtonId

            if (rbId is Int) {
                putStringInSP(view, rbId, prefs, "wishes")
            }

            val kCal = kCalFormula(
                prefs?.getString("sex", 0.toString())!!,
                prefs?.getString("age", 0.toString())!!,
                prefs?.getString("height", 0.toString())!!,
                prefs?.getString("weight", 0.toString())!!,
                prefs?.getString("activity", 0.toString())!!,
                prefs?.getString("wishes", 0.toString())!!,
            )
            prefs?.edit()?.putInt("kcal", kCal)?.apply()
            val water = waterFormula(
                prefs?.getString("sex", 0.toString())!!,
                prefs?.getString("weight", 0.toString())!!,
                prefs?.getString("activity", 0.toString())!!,
            )

            prefs?.edit()?.putInt("water", water)?.apply()

            prefs?.edit()?.putBoolean("hasVisited", true)?.apply()
                     findNavController().navigate(R.id.action_chooseYourWishes_to_nav_graph)

        }
    }

    private fun findView(view: View) {
        rgChooseWishes = view.findViewById(R.id.rv_wishes_radio_group)
        btnGo = view.findViewById(R.id.go_btn)
    }
}