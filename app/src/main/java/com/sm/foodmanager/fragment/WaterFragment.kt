package com.sm.foodmanager.fragment

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.sm.foodmanager.MainActivity
import com.sm.foodmanager.R
import com.sm.foodmanager.databinding.FragmentWaterBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class WaterFragment : Fragment(R.layout.fragment_water) {
    companion object{
        lateinit var seriesWater: BarGraphSeries<DataPoint>
        var QuantityWater=0
    }
    private val CHANNEL_ID = "channel_id_01"

    private var progressOnProgressBar = 0
    private var curProgress = 0.0
    private  var prefs : SharedPreferences?=null
    private var binding: FragmentWaterBinding? = null
    private var date: String? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentWaterBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        prefs = context?.getSharedPreferences(
            "FoodManager", Context.MODE_PRIVATE
        )
        seriesWater = BarGraphSeries<DataPoint>()
        binding?.tvNeededProgress?.text=prefs?.getInt("water", 0).toString()
        createNotificationChannel()

        val context = this.requireContext()
        val alarmManager = this.activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        binding?.bAddWater?.setOnClickListener {
            val textNumber = binding?.etWater?.text?.toString() ?: ""
            val validNumber = textNumber == ""
            if (validNumber) {
                showMessage(getString(R.string.empty_water))
                return@setOnClickListener
            } else {
                progressOnProgressBar += Integer.parseInt(binding?.etWater?.text?.toString())
                val addedMl = (Integer.parseInt(binding?.etWater?.text?.toString())).toDouble()
                val curMl = Integer.parseInt(binding?.tvCurWater?.text?.toString()).toDouble()
                val neededMl =
                    (Integer.parseInt(binding?.tvNeededProgress?.text?.toString())).toDouble()
                curProgress = ((((addedMl + curMl) / neededMl) * 100))
                curProgress = (curProgress * 100.0).roundToInt() / 100.0
                updateProgressBar()
                binding?.etWater?.setText("")
            }
            val intent = Intent(context, Receiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            Log.d("MainActivity", " Create : " + Date().toString())
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 7200000, pendingIntent)
        }
        binding?.bPrevFragment?.setOnClickListener{
            findNavController().navigate(R.id.action_waterFragment_to_kcalFragment)
        }
        binding?.bNextFragment?.setOnClickListener{
            findNavController().navigate(R.id.action_waterFragment_to_profileFragment)
        }

    }

    private fun createNotificationChannel(){
        if(Build.VERSION.SDK_INT >=  Build.VERSION_CODES.O){
            val name = "Notification title"
            val descriptionText = "Notification description"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager = this.activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun updateProgressBar() {
        binding?.progressBarWater?.progress = curProgress.toInt()
        binding?.tvProgressPercent?.text = "${curProgress}%"
        binding?.tvCurWater?.text = "$progressOnProgressBar"

        QuantityWater+=1
        seriesWater.appendData(DataPoint(QuantityWater.toDouble(), progressOnProgressBar.toDouble()) , true, 100)

    }

    private fun showMessage(message: String) {
        activity?.findViewById<View>(android.R.id.content)?.also {
            Snackbar.make(
                it,
                message,
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    override fun onPause() {
        super.onPause()
        prefs?.edit()?.putInt("progressOnBarWater", binding?.progressBarWater?.progress!!)?.apply()
        prefs?.edit()?.putString("viewProgressTvWater", binding?.tvProgressPercent?.text.toString())?.apply()
        prefs?.edit()?.putString("neededWater",binding?.tvNeededProgress?.text.toString())?.apply()
        prefs?.edit()?.putString("tvCurWater", binding?.tvCurWater?.text.toString())?.apply()
        curProgress= binding?.progressBarWater?.progress?.toDouble()!!
        progressOnProgressBar= Integer.parseInt(binding?.tvCurWater?.text.toString())
        val calendar = Calendar.getInstance()
        val smf = SimpleDateFormat("dd-MM-yyyy")
        date = smf.format(calendar.time)
        prefs?.edit()?.putString("timeOfLastLaunchWater", date)?.apply()
    }

    override fun onResume() {
        super.onResume()

        val calendar = Calendar.getInstance()
        val smf = SimpleDateFormat("dd-MM-yyyy")
        val currentDate = smf?.format(calendar.time)

        val pastDate = prefs?.getString("timeOfLastLaunchWater", "0")
        if (pastDate == currentDate) {
            binding?.progressBarWater?.progress = (prefs?.getInt("progressOnBarWater", 0)!!)
            binding?.tvProgressPercent?.text = "${(prefs?.getString("viewProgressTvWater", "0"))}"
            binding?.tvCurWater?.setText(prefs?.getString("tvCurWater", "0"))
            binding?.tvNeededProgress?.text=prefs?.getString("neededWater","0")
            curProgress = binding?.progressBarWater?.progress?.toDouble()!!
            progressOnProgressBar = Integer.parseInt(binding?.tvCurWater?.text.toString())

        }else{
            curProgress=0.0
            progressOnProgressBar=0
            binding?.progressBarWater?.progress = 0
            binding?.tvProgressPercent?.text = "0%"
            binding?.tvCurWater?.text = "0"
        }
    }


    class Receiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            Log.d("MainActivity", " Receiver : " + Date().toString())

            val CHANNEL_ID = "channel_id_01"
            val notificationId = 101

            val intent = Intent(context, MainActivity::class.java).apply{
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

            val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_action_jojowater)
            val bitmapLargeIcon = BitmapFactory.decodeResource(context.resources, R.drawable.ic_action_watercup)

            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("You haven\'t drunk water for 2 hours")
                .setContentText("Drink something to survive")
                .setLargeIcon(bitmapLargeIcon)
                .setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            with(NotificationManagerCompat.from(context)){
                notify(notificationId, builder.build())
            }
        }
    }
}


