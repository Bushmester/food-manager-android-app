package com.sm.foodmanager.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sm.foodmanager.R
import com.sm.foodmanager.utils.putStringInSP

class ChooseYourActivity : Fragment(R.layout.fragment_choose_your_activity) {

    private var rgChooseAct: RadioGroup? = null
    private var btnNext: AppCompatButton? = null

    private var prefs: SharedPreferences? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView(view)
        initListeners(view)
    }

    private fun initListeners(view: View) {
        prefs = context?.getSharedPreferences("FoodManager", Context.MODE_PRIVATE)

        btnNext?.setOnClickListener {
            val rbId: Int? = rgChooseAct?.checkedRadioButtonId

            if (rbId is Int) {
                putStringInSP(view, rbId, prefs, "activity")
            }
            findNavController().navigate(R.id.action_chooseYourActivity_to_chooseYourWishes)
        }
    }

    private fun findView(view: View) {
        rgChooseAct = view.findViewById(R.id.act_radio_group)
        btnNext = view.findViewById(R.id.next_btn)
    }
}