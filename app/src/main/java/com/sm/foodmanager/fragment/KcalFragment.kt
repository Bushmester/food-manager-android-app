package com.sm.foodmanager.fragment
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.AlarmClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.sm.foodmanager.MainActivity
import com.sm.foodmanager.R
import com.sm.foodmanager.databinding.FragmentChooseYourWishesBinding
import com.sm.foodmanager.databinding.FragmentKcalBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt
class KcalFragment: Fragment(R.layout.fragment_kcal) {
    companion object{
        lateinit var seriesKcal: BarGraphSeries<DataPoint>
        var QuantityKcal=0
    }

    private val CHANNEL_ID = "channel_id_02"

    private var progressOnProgressBar = 0
    private var curProgress = 0.0
    private var prefs: SharedPreferences? = null
    private var binding: FragmentKcalBinding? = null
    private var date: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentKcalBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        prefs = context?.getSharedPreferences(
            "FoodManager", Context.MODE_PRIVATE
        )
        binding?.tvNeeededProg?.text=prefs?.getInt("kcal", 0).toString()
        seriesKcal=BarGraphSeries<DataPoint>()
        createNotificationChannel()

        val context = this.requireContext()
        val alarmManager = this.activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        binding?.bAdd?.setOnClickListener {
            if (progressOnProgressBar > Integer.parseInt(binding?.tvNeeededProg?.text.toString())) {
                showMessage(getString((R.string.alot_kcal)))
            }
            val textNumber = binding?.etKcal?.text?.toString() ?: ""
            val validNumber = textNumber == ""
            if (validNumber) {
                showMessage(getString(R.string.empty_kall))
                return@setOnClickListener
            } else {
                progressOnProgressBar += Integer.parseInt(binding?.etKcal?.text?.toString())
                val addedCal = (Integer.parseInt(binding?.etKcal?.text?.toString())).toDouble()
                val curCal = Integer.parseInt(binding?.tvCurKcal?.text?.toString()).toDouble()
                val neededCal =
                    (Integer.parseInt(binding?.tvNeeededProg?.text?.toString())).toDouble()
                curProgress = ((((addedCal + curCal) / neededCal) * 100))
                curProgress = (curProgress * 100.0).roundToInt() / 100.0
                updateProgressBar()
                binding?.etKcal?.setText("")
            }
            val intent = Intent(context, KcalFragment.Receiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            Log.d("MainActivity", " Create : " + Date().toString())
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 14400000, pendingIntent)
        }
        binding?.bNextFragment?.setOnClickListener {
            findNavController().navigate(R.id.action_kcal_fragment_to_waterFragment)
        }
    }

    private fun createNotificationChannel(){
        if(Build.VERSION.SDK_INT >=  Build.VERSION_CODES.O){
            val name = "Notification title"
            val descriptionText = "Notification description"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager = this.activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun updateProgressBar() {
        binding?.progressBar?.progress = curProgress.toInt()
        binding?.viewProgressTv?.text = "${curProgress}%"
        binding?.tvCurKcal?.text = "$progressOnProgressBar"
        QuantityKcal +=1
        seriesKcal.appendData(DataPoint(QuantityKcal.toDouble(), progressOnProgressBar.toDouble()) , true, 100)
    }

    private fun showMessage(message: String) {
        activity?.findViewById<View>(android.R.id.content)?.also {
            Snackbar.make(
                it,
                message,
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    override fun onPause() {
        super.onPause()
        prefs?.edit()?.putInt("progressOnBarKcal", binding?.progressBar?.progress!!)?.apply()
        prefs?.edit()?.putString("viewProgressTvKcal", binding?.viewProgressTv?.text.toString())
            ?.apply()
        prefs?.edit()?.putString("tvCurKcal", binding?.tvCurKcal?.text.toString())?.apply()
        prefs?.edit()?.putString("neededKcal",binding?.tvNeeededProg?.text.toString())?.apply()
        curProgress = binding?.progressBar?.progress?.toDouble()!!
        progressOnProgressBar = Integer.parseInt(binding?.tvCurKcal?.text.toString())

        val calendar = Calendar.getInstance()
        val smf = SimpleDateFormat("dd-MM-yyyy")
        date = smf.format(calendar.time)
        prefs?.edit()?.putString("timeOfLastLaunchKcal", date)?.apply()
    }

    override fun onResume() {
        super.onResume()
        val calendar = Calendar.getInstance()
        val smf = SimpleDateFormat("dd-MM-yyyy")
        val currentDate = smf?.format(calendar.time)
        val pastDate = prefs?.getString("timeOfLastLaunchKcal", "0")
        if (pastDate == currentDate) {
            binding?.progressBar?.progress = (prefs?.getInt("progressOnBarKcal", 0)!!)
            binding?.viewProgressTv?.text = "${(prefs?.getString("viewProgressTvKcal", "0"))}"
            binding?.tvCurKcal?.setText(prefs?.getString("tvCurKcal", "0"))
            binding?.tvNeeededProg?.text=prefs?.getString("neededKcal","0")
            curProgress = binding?.progressBar?.progress?.toDouble()!!
            progressOnProgressBar = Integer.parseInt(binding?.tvCurKcal?.text.toString())
        } else {
            binding?.progressBar?.progress = 0
            binding?.viewProgressTv?.text = "0%"
            binding?.tvCurKcal?.text = "0"
            curProgress = 0.0
            progressOnProgressBar = 0
        }
    }


    class Receiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            Log.d("MainActivity", " Receiver : " + Date().toString())

            val CHANNEL_ID = "channel_id_02"
            val notificationId = 102

            val intent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

            val bitmap =
                BitmapFactory.decodeResource(context.resources, R.drawable.ic_action_jojofood)
            val bitmapLargeIcon =
                BitmapFactory.decodeResource(context.resources, R.drawable.ic_action_food)

            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("You haven\'t eat for 4 hours")
                .setContentText("Eat something to survive")
                .setLargeIcon(bitmapLargeIcon)
                .setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)

            with(NotificationManagerCompat.from(context)) {
                notify(notificationId, builder.build())
            }
        }
    }
}



