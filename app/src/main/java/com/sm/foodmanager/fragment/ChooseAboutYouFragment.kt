package com.sm.foodmanager.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import com.sm.foodmanager.R

class ChooseAboutYou : Fragment(R.layout.fragment_choose_about_you) {

    private var etAge: EditText? = null
    private var tiAge: TextInputLayout? = null
    private var etHeight: EditText? = null
    private var tiHeight: TextInputLayout? = null
    private var etWeight: EditText? = null
    private var tiWeight: TextInputLayout? = null
    private var btnNext: AppCompatButton? = null

    private var prefs: SharedPreferences? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView(view)
        initListeners()
    }

    private fun initListeners() {
        prefs = context?.getSharedPreferences("FoodManager", Context.MODE_PRIVATE)

        btnNext?.setOnClickListener {

            prefs?.edit()?.putString("age", etAge?.text?.toString())?.apply()
            prefs?.edit()?.putString("height", etHeight?.text?.toString())?.apply()
            prefs?.edit()?.putString("weight", etWeight?.text?.toString())?.apply()

            findNavController().navigate(R.id.action_chooseAboutYou_to_chooseYourActivity)
        }
    }

    private fun findView(view: View) {
        etAge = view.findViewById(R.id.et_age)
        tiAge = view.findViewById(R.id.ti_age)
        etHeight = view.findViewById(R.id.et_height)
        tiHeight = view.findViewById(R.id.ti_height)
        etWeight = view.findViewById(R.id.et_weight)
        tiWeight = view.findViewById(R.id.ti_weight)
        btnNext = view.findViewById(R.id.next_btn)
    }
}