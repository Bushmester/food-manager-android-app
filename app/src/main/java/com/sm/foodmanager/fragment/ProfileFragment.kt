package com.sm.foodmanager.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.sm.foodmanager.R

class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private var btnEditAct: Button? = null
    private var btnEditInfo: Button? = null
    private var btnEditWish: Button? = null
    private var btnPrevFragment: Button? = null
    private var btnStat: Button? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findView(view)
        initListeners()
    }

    private fun findView(view: View) {
        btnEditAct = view.findViewById(R.id.btn_edit_act)
        btnEditInfo = view.findViewById(R.id.btn_edit_info)
        btnEditWish= view.findViewById(R.id.btn_edit_wish)
        btnPrevFragment= view.findViewById(R.id.b_prevFragment)
        btnStat = view.findViewById(R.id.btn_stat)
    }

    private fun initListeners() {

        btnPrevFragment?.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_waterFragment)
        }

        btnEditAct?.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_editActivityFragment)
        }

        btnEditInfo?.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_editAboutYouFragment)
        }

        btnEditWish?.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_editWishesFragment)
        }

        btnEditWish?.setOnClickListener {
            findNavController().navigate(R.id.action_profileFragment_to_editWishesFragment)
        }
    }
}

