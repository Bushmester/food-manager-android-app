package com.sm.foodmanager.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatButton
import androidx.navigation.fragment.findNavController
import com.sm.foodmanager.R
import com.sm.foodmanager.utils.putStringInSP

class ChooseSexFragment : Fragment(R.layout.fragment_choose_sex) {

    private var rgChooseSex: RadioGroup? = null
    private var btnNext: AppCompatButton? = null

    private var prefs: SharedPreferences? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findView(view)
        initListeners(view)
    }

    @SuppressLint("Recycle")
    private fun initListeners(view: View) {
        prefs = context?.getSharedPreferences("FoodManager", Context.MODE_PRIVATE)

        btnNext?.setOnClickListener {
            val rbId: Int? = rgChooseSex?.checkedRadioButtonId

            if (rbId is Int) {
                putStringInSP(view, rbId, prefs, "sex")
            }

            findNavController().navigate(R.id.action_chooseSexFragment_to_chooseAboutYou)
        }
    }

    private fun findView(view: View) {
        rgChooseSex = view.findViewById(R.id.sex_radio_group)
        btnNext = view.findViewById(R.id.next_btn)
    }
}