package com.sm.foodmanager

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class LauncherActivity : AppCompatActivity() {

    private var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)

        prefs = getSharedPreferences("FoodManager", Context.MODE_PRIVATE)
        val hasVisited = prefs?.getBoolean("hasVisited", false)

        if (hasVisited!!) {
            val intent =  Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        else {
            val intent =  Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

    }
}