package com.sm.foodmanager.utils

import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.RadioButton

fun putStringInSP(view: View?, rbId: Int, prefs: SharedPreferences?, name: String) {
    val text: RadioButton = view?.findViewById(rbId)!!

    prefs?.edit()?.putString(name, text.text.toString())?.apply()
}