package com.sm.foodmanager.utils

import com.sm.foodmanager.const.SexValues
import com.sm.foodmanager.const.Activity
import com.sm.foodmanager.const.Wishes

fun kCalFormula(sex: String, age: String, height: String, weight: String, activity: String, wishes: String,): Int {
    var currentKcal =
                SexValues[sex]?.get("sex arg")!! +
                (SexValues[sex]?.get("weight arg")!! * weight.toInt()) +
                (SexValues[sex]?.get("height arg")!! * height.toInt()) -
                (SexValues[sex]?.get("age arg")!! * age.toInt())

    currentKcal *= Activity[activity]!!
    currentKcal += Wishes[wishes]!!

    return currentKcal.toInt()
}

fun waterFormula(sex: String, weight: String, activity: String): Int {
    var currentWater =
        SexValues[sex]?.get("water first")!! * weight.toInt() +
        SexValues[sex]?.get("water second")!! * Activity[activity]!!

    currentWater *= 1000

    return currentWater.toInt()
}
