package com.sm.foodmanager.const

val SexValues: HashMap<String, HashMap<String, Float>> = hashMapOf(
    "Male" to hashMapOf(
                "sex arg" to 88.36F,
                "weight arg" to 13.4F,
                "height arg" to 4.8F,
                "age arg" to 5.7F,
                "water first" to 0.03F,
                "water second" to 0.5F,
    ),
    "Female" to hashMapOf(
                "sex arg" to 447.6F,
                "weight arg" to 9.2F,
                "height arg" to 3.1F,
                "age arg" to 4.3F,
                "water first" to 0.025F,
                "water second" to 0.4F,
    ),
)