package com.sm.foodmanager.const

val Wishes: HashMap<String, Int> = hashMapOf(
    "Gain weight" to 300,
    "Save weight" to 0,
    "Lose weight" to -300,
)