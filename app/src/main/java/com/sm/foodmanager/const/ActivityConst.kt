package com.sm.foodmanager.const

val Activity: HashMap<String, Float> = hashMapOf(
    "No loads" to 1.2F,
    "Light" to 1.375F,
    "Average" to 1.55F,
    "Intensive" to 1.725F,
    "Challenging" to 1.9F,
)