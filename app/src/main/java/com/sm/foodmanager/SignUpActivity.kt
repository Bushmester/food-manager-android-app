package com.sm.foodmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

class SignUpActivity : AppCompatActivity() {

    private lateinit var controller: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        controller = (supportFragmentManager
            .findFragmentById(R.id.host_sign_up_fragment) as NavHostFragment)
            .navController

    }

    override fun onSupportNavigateUp(): Boolean = controller.navigateUp()
}